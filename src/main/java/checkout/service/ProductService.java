package checkout.service;

import checkout.model.Product;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProductService {
	Product findById(Long id);
    List<Product> findAll();
    void save(Product product);
}
