package checkout.service;

import checkout.dto.OrderDto;
import checkout.dto.ProductDto;

import java.util.List;

public interface OrderService {

	OrderDto findById(Long id);

	OrderDto findByNumber(String number);

	OrderDto create(List<ProductDto> orderDto);

    List<OrderDto> findAll();
}
