package checkout.service;

import checkout.dao.OrderDao;
import checkout.dao.OrderProductDao;
import checkout.dao.ProductDao;
import checkout.dto.OrderDto;
import checkout.dto.ProductDto;
import checkout.error.NotFoundException;
import checkout.model.Order;
import checkout.model.OrderProduct;
import checkout.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public OrderDto findById(Long id) {
        Order order = orderDao.findById(id);
        Optional.ofNullable(order).orElseThrow(() -> new NotFoundException("Order not found"));
        return OrderDto.toDto(order);
    }

    @Override
    @Transactional
    public OrderDto findByNumber(String number) {
        Order order = orderDao.findByNumber(number);
        Optional.ofNullable(order).orElseThrow(() -> new NotFoundException("Order not found"));
        return OrderDto.toDto(order);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public OrderDto create(List<ProductDto> productDtos) {
        HashMap<Long, Integer> quantityMap = getQuantityMap(productDtos);
        Collection<Product> products = productDao.findAllByIds(quantityMap.keySet());

        Order order = new Order();

        double fullPrice = 0;
        for (Product p : products) {
            Integer quantity = quantityMap.get(p.getId());

            if(quantity == null) {
                continue;
            }

            OrderProduct op = new OrderProduct(order, p, quantity);
            order.getOrderProducts().add(op);

            fullPrice += p.getPrice() * quantity;
        }
        orderDao.save(order);
        userService.debit(fullPrice);

        return OrderDto.toDto(order);
    }

    @Override
    @Transactional
    public List<OrderDto> findAll() {
        List<Order> orders = orderDao.findAll();
        List<OrderDto> orderDtos = orders.stream().map(o -> OrderDto.toDto(o)).collect(Collectors.toList());
        return orderDtos;
    }

    private HashMap<Long, Integer> getQuantityMap(List<ProductDto> productDtos) {
        HashMap<Long, Integer> map = new HashMap<>();

        for (ProductDto productDto : productDtos) {
            Long id = productDto.id;
            if (map.containsKey(id)) {
                map.put(id, map.get(id) + 1);
            } else {
                map.put(id, 1);
            }
        }
        return map;
    }

}
