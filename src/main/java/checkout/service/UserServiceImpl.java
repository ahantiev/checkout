package checkout.service;

import checkout.dao.UserDao;
import checkout.error.BadRequestException;
import checkout.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public void debit(double amount) {
        User user = userDao.findById(User.DEFAULT_USER_ID);
        if(user.getBalance() < amount) {
            throw new BadRequestException("Not enough money");
        }
        user.setBalance(user.getBalance() - amount);
        userDao.save(user);
    }
}
