package checkout.service;

public interface UserService {

	void debit(double amount);
}
