package checkout.service;

import checkout.dao.ProductDao;
import checkout.dao.UserDao;
import checkout.model.Product;
import checkout.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service("fixtureService")
public class FixtureService {

    @Autowired
    private ProductDao productDao;
    @Autowired
    private UserDao userDao;

    @Transactional
    public void createFixture(int productCount) {
        User user = new User(1000.0);
        userDao.save(user);

        for (int i = 1; i < productCount; i++) {
            double randomPrice = Math.random() * 50.00;
            randomPrice = new BigDecimal(randomPrice).setScale(2, RoundingMode.HALF_UP).doubleValue();

            Product product = new Product("article" + i, "Product #" + i, randomPrice);
            productDao.save(product);
        }
    }
}
