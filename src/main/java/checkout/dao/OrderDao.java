package checkout.dao;

import checkout.model.Order;

public interface OrderDao extends AbstractDao<Long, Order> {
    Order findByNumber(String number);
}
