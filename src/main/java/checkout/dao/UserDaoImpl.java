package checkout.dao;

import checkout.model.User;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl extends AbstractDaoImpl<Long, User> implements UserDao {

}
