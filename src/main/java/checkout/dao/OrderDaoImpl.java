package checkout.dao;

import checkout.model.Order;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("orderDao")
public class OrderDaoImpl extends AbstractDaoImpl<Long, Order> implements OrderDao {

    @Override
    public Order findByNumber(String number) {
        Criteria criteria = getSession().createCriteria(Order.class);
        return (Order) criteria.add(Restrictions.eq("number", number))
                .uniqueResult();


    }
}
