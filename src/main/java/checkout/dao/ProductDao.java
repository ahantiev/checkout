package checkout.dao;

import checkout.model.Product;

import java.util.Collection;
import java.util.List;

public interface ProductDao extends AbstractDao<Long, Product> {
    List<Product> findAllByIds(Collection<Long> ids);
}
