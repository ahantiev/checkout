package checkout.dao;

import checkout.model.OrderProduct;

public interface OrderProductDao extends AbstractDao<Long, OrderProduct> {
}
