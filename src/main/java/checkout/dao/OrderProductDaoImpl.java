package checkout.dao;

import checkout.model.OrderProduct;
import org.springframework.stereotype.Repository;

@Repository("orderProductDao")
public class OrderProductDaoImpl extends AbstractDaoImpl<Long, OrderProduct> implements OrderProductDao {

}
