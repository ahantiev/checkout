package checkout.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractDaoImpl<PK extends Serializable, T> implements AbstractDao<PK, T> {

	@Autowired
	private SessionFactory sessionFactory;

	private final Class<T> clazz;
	
	@SuppressWarnings("unchecked")
	public AbstractDaoImpl(){
		this.clazz =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	protected Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	@Override
	public T findById(PK key) {
		return (T) getSession().get(clazz, key);
	}

	@Override
	public void save(T entity) {
		getSession().save(entity);
	}

	public void delete(T entity) {
		getSession().delete(entity);
	}

	@Override
	public List<T> findAll() {
		final Session session = sessionFactory.getCurrentSession();
		final Criteria crit = session.createCriteria(clazz);
		return crit.list();
	}
}
