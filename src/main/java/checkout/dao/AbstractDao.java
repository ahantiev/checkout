package checkout.dao;

import java.io.Serializable;
import java.util.List;

public interface AbstractDao<PK extends Serializable, T> {
	T findById(PK id);

	void save(T entity);

	List<T> findAll();
}
