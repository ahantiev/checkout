package checkout.dao;

import checkout.model.Product;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDaoImpl<Long, Product> implements ProductDao {

    @Override
    public List<Product> findAllByIds(Collection<Long> ids) {
        Query query = getSession().createQuery("FROM Product p WHERE p.id IN (:ids)");
        query.setParameterList("ids", ids);
        return query.list();
    }
}
