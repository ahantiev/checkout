package checkout.dao;

import checkout.model.User;

public interface UserDao extends AbstractDao<Long, User> {

}
