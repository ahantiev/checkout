package checkout.dto;

import checkout.model.Order;
import checkout.model.OrderProduct;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class OrderDto {
    public Long id;
    public String number;
    public Order.Status status;
    public DateTime createdAt;
    public List<ProductDto> products = new ArrayList<>();

    public OrderDto(Long id, String number) {
        this.id = id;
        this.number = number;
    }

    public static OrderDto toDto(Order order) {
        OrderDto orderDto = new OrderDto(order.getId(), order.getNumber());
        orderDto.status = order.getStatus();
        orderDto.createdAt = order.getCreatedAt();
        orderDto.products.addAll(getProducts(order.getOrderProducts()));
        return orderDto;
    }

    private static List<ProductDto> getProducts(Set<OrderProduct> orderProductSet) {
        return orderProductSet.stream()
                .flatMap(orderProduct -> orderProduct.getProducts().stream())
                .map(ProductDto::toDto)
                .collect(Collectors.toList());
    }

}
