package checkout.dto;

import checkout.model.Product;

public class ProductDto {
    public Long id;
    public String article;
    public String name;
    public Double price;

    public ProductDto() {
    }

    public ProductDto(Long id, String article, String name, Double price) {
        this.id = id;
        this.article = article;
        this.name = name;
        this.price = price;
    }

    public static ProductDto toDto(Product order) {
        return new ProductDto(order.getId(), order.getArticle(), order.getName(), order.getPrice());
    }

}
