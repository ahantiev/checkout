package checkout.controller;

import checkout.model.Product;
import checkout.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping
public class ProductController {
    @Autowired
    ProductService productService;

    @RequestMapping(value = { "/products"}, method = RequestMethod.GET)
    public @ResponseBody List<Product> findAll() {
        return productService.findAll();
    }
}
