package checkout.controller;

import checkout.dto.OrderDto;
import checkout.dto.ProductDto;
import checkout.error.BadRequestException;
import checkout.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping
public class OrderController {

	@Autowired
	OrderService orderService;

	@RequestMapping(value = { "/orders/{number}"}, method = RequestMethod.GET)
	public @ResponseBody OrderDto findByNumber(@PathVariable String number) {
		return orderService.findByNumber(number);
	}

	@RequestMapping(value = { "/orders"}, method = RequestMethod.GET)
	public @ResponseBody List<OrderDto> findAll() {
		return orderService.findAll();
	}

	@RequestMapping(value = { "/orders"}, method = RequestMethod.POST)
	public @ResponseBody OrderDto createOrder(@RequestBody List<ProductDto> products) {
		if(products.size() == 0) {
			throw new BadRequestException("Invalid params");
		}
		return orderService.create(products);
	}

}
