package checkout.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CH_ORDER_PRODUCT")
public class OrderProduct {
    @EmbeddedId
    private Id id = new Id();

    @ManyToOne
    @MapsId(value = "orderId")
    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    private Order order;

    @ManyToOne
    @MapsId(value = "productId")
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private Product product;

    @Column(name = "quantity")
    private int quantity = 1;

    public OrderProduct() {
    }

    public OrderProduct(Order order, Product product, int quantity) {
        this.order = order;
        this.product = product;
        this.quantity = quantity;
    }

    @Transient
    public List<Product> getProducts() {
        List<Product> list = new ArrayList<>();
        for (int i = 0; i < this.getQuantity(); i++) {
            list.add(this.product);
        }
        return list;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderProduct that = (OrderProduct) o;

        if (!id.equals(that.id)) return false;
        if (!order.equals(that.order)) return false;
        return product.equals(that.product);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + order.hashCode();
        result = 31 * result + product.hashCode();
        return result;
    }

    @Embeddable
    static class Id implements Serializable {
        private static final long serialVersionUID = -2428662152218341557L;

        @Column(name = "order_id")
        private Long orderId;
        @Column(name = "product_id")
        private Long productId;

        public Id() {
        }

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Long getProductId() {
            return productId;
        }

        public void setProductId(Long productId) {
            this.productId = productId;
        }
    }

}
