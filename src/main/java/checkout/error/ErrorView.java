package checkout.error;

import java.io.Serializable;

public class ErrorView implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String message;
    private final String description;
    ErrorView(String message) {
        this(message, null);
    }
    ErrorView(String message, String description) {
        this.message = message;
        this.description = description;
    }

    public String getMessage() {
        return message;
    }
    public String getDescription() {
        return description;
    }
}